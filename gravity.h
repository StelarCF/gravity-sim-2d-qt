#ifndef GRAVITY_H
#define GRAVITY_H

#include <QMainWindow>
#include <QTimer>
#include <QtGui>
#include <QScrollBar>
#include <QTreeWidgetItem>
#include <QShortcut>
#include <QElapsedTimer>
#include <cmath>
#include <list>
#include <vector>
#include <string>
#include "cpptoml.h"

typedef std::vector<QString> argList;

class CosmicObject;

namespace Ui {
class Gravity;
}

class Gravity : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit Gravity(QWidget *parent = 0);
    void paintEvent(QPaintEvent *);
    void pas_fizica(double deltaT, bool tracktrail);
    void calculatestepnr();
    void loadSettings();
    void selectObject(argList args);
    void centerObject(argList args);
    void clear(argList args);
    void addobject(argList args);
    void add_elastic_tie(argList args);
    void step();
    CosmicObject *getObject(QString s);
    ~Gravity();
    
private:
    Ui::Gravity *ui;

private slots:
    void miau();
    void intr_fizica();
    void on_command();
    void on_pauseButton_clicked();
    void on_nextStepButton_clicked();
};

#endif // GRAVITY_H
