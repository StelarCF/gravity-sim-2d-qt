#include "gravity.h"
#include "ui_gravity.h"
using std::string;

// git add *
// git commit -a -m "Mesajul tau aici"
// git push

// git pull

//#define R4FORCE

#define GRAVITY_CONSTANT 50000.0

#ifdef R4FORCE
#define OTHER_CONSTANT 50000000.0
#endif

#define STEP_MS 25
#define TRAIL_NUMBERS 10000
#define COEF_COEF 200.0
#define STEP_MAX_VALUE 20000

QTimer *t_fizica;

class vec2f {
public:
    double x, y;
    double operator[](int i) {
        if(i == 0)
            return x;
        else return y;
    }
    double size() {
        return sqrt((x * x) + (y * y));
    }
    vec2f operator=(vec2f other) {
        x = other.x;
        y = other.y;
        return *this;
    }
    vec2f operator+(vec2f other) {
        vec2f rezultat;
        rezultat.x = x + other.x;
        rezultat.y = y + other.y;
        return rezultat;
    }
    vec2f operator-(vec2f other) {
        vec2f rezultat;
        rezultat.x = x - other.x;
        rezultat.y = y - other.y;
        return rezultat;
    }
    double distance(vec2f other) {
        return (*this - other).size();
    }
    double sizesq() {
        return x * x + y * y;
    }
    double distancesq(vec2f other) {
        return (*this - other).sizesq();
    }
    void toUnit() {
        double l;
        l = this->size();
        x /= l;
        y /= l;
    }
    vec2f operator*(double scalar) {
        vec2f rez;
        rez.x = scalar * x;
        rez.y = scalar * y;
        return rez;
    }
};

double mindinsubstep;

struct etie_t {
    double ke, xr;
    CosmicObject *other;
};

class CosmicObject {
public:
    std::list<vec2f> prev_poz;
    std::vector<etie_t> elastic_ties;
    vec2f pozitie, viteza;
    string name;
    double mass, radius;
    bool fictive, destroyed;
    QTreeWidgetItem *qtwi;
    CosmicObject() {
        qtwi = new QTreeWidgetItem();
        destroyed = false;
    }

    // Supposed to be called once for each pair per step
    void interact_gravity(CosmicObject &other, double deltaT) {
        double distsq;
        double force;
        vec2f directie;
        if(fictive || other.fictive || destroyed || other.destroyed)
            return;
        distsq = pozitie.distancesq(other.pozitie);
        double dist = sqrt(distsq);
        if(dist < mindinsubstep)
            mindinsubstep = dist;

        force = mass * other.mass * GRAVITY_CONSTANT / distsq;
#ifdef R4FORCE
        force -= mass * other.mass * OTHER_CONSTANT / (distsq * distsq);
#endif

        directie = other.pozitie - pozitie;
        directie.toUnit();

        viteza = viteza + directie * (force * deltaT / mass);
        other.viteza = other.viteza + directie * (force * deltaT * -1 / other.mass);
    }
    void check_collision(CosmicObject &other) {
        if(fictive || other.fictive || destroyed || other.destroyed)
            return;
        if((other.pozitie - pozitie).size() < radius + other.radius) {
            other.fictive = other.destroyed = true;
            viteza = (viteza * mass + other.viteza * other.mass) * (1.0 / (mass + other.mass));
            mass = mass + other.mass;
            radius = sqrt(radius * radius + other.radius * other.radius); // va trebui in final de ordin 3
            //name = name + " - " + other.name;
        }
    }

    void do_step(double deltaT, bool tracktrail) {
        int i;
        if(fictive)
            return;
        if(tracktrail)
            prev_poz.push_back(pozitie);
        for(i = 0; i < elastic_ties.size(); i++) {
            if(elastic_ties[i].other->fictive)
                continue;
            double dx;
            vec2f force;
            dx = elastic_ties[i].xr - pozitie.distance(elastic_ties[i].other->pozitie);
            force = pozitie - elastic_ties[i].other->pozitie;
            force.toUnit();
            force = force * (dx * elastic_ties[i].ke);
            viteza = viteza + force * (deltaT / mass);
            elastic_ties[i].other->viteza = elastic_ties[i].other->viteza + force * (deltaT * -1.0 / elastic_ties[i].other->mass);
        }
        this->pozitie = this->pozitie + this->viteza * deltaT;
        if(prev_poz.size() > TRAIL_NUMBERS)
            prev_poz.pop_front();
    }
};

CosmicObject c1, c2, c3;
std::vector<CosmicObject> cosmicObjects;
bool paused;

Gravity::Gravity(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Gravity)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(miau()));

    connect(ui->commandEdit, SIGNAL(returnPressed()), this, SLOT(on_command()));
    connect(ui->enterCommandButton, SIGNAL(clicked()), this, SLOT(on_command()));

    t_fizica = new QTimer(this);
    t_fizica->start(STEP_MS); // parametrul da numarul de milisecunde la care are loc o intrerupere

    loadSettings();

    connect(t_fizica, SIGNAL(timeout()), this, SLOT(intr_fizica()));

    ui->objectList->header()->resizeSections(QHeaderView::Stretch);

    new QShortcut(QKeySequence(Qt::Key_P), this, SLOT(on_pauseButton_clicked()));
    new QShortcut(QKeySequence(Qt::Key_S), this, SLOT(on_nextStepButton_clicked()));
}

// Settings cam urat, puteam face mai frumos recursiv... meh
void Gravity::loadSettings() {
    // global
    cpptoml::table g = cpptoml::parse_file("settings.cfg");
    auto it1 = g.begin();
    while(it1 != g.end()) {
        if(it1->first == "objects" && it1->second->is_table()) {
            // objects
            auto g2 = g.get_table(it1->first);
            auto it2 = g2->begin();
            while(it2 != g2->end()) {
                if(it2->second->is_table()) {
                    // specific object (name = it2->first)
                    CosmicObject ccrt;
                    ui->objectList->addTopLevelItem(ccrt.qtwi);
                    ccrt.fictive = false;
                    auto g3 = g2->get_table(it2->first);
                    ccrt.name = it2->first;
                    auto it3 = g3->begin();
                    while(it3 != g3->end()) {
                        if(it3->first == "mass") {
                            auto val = (it3->second->as<double>())->get();
                                ccrt.mass = val;
                        }
                        if(it3->first == "px") {
                            auto val = (it3->second->as<double>())->get();
                                ccrt.pozitie.x = val;
                        }
                        if(it3->first == "py") {
                            auto val = (it3->second->as<double>())->get();
                                ccrt.pozitie.y = val;
                        }
                        if(it3->first == "vx") {
                            auto val = (it3->second->as<double>())->get();
                                ccrt.viteza.x = val;
                        }
                        if(it3->first == "vy") {
                            auto val = (it3->second->as<double>())->get();
                                ccrt.viteza.y = val;
                        }
                        if(it3->first == "radius") {
                            auto val = (it3->second->as<double>())->get();
                                ccrt.radius = val;
                        }
                        if(it3->first == "fictive")
                            ccrt.fictive = true;
                        it3++;
                    }
                    cosmicObjects.push_back(ccrt);
                }
                it2++;
            }
        } else if(it1->first == "springs" && it1->second->is_table()) {
                // springs
                auto g2 = g.get_table(it1->first);
                auto it2 = g2->begin();
                while(it2 != g2->end()) {
                    if(it2->second->is_table()) {
                        auto g3 = g2->get_table(it2->first);
                        auto it3 = g3->begin();
                        etie_t tn;
                        QString first, second;
                        while(it3 != g3->end()) {
                            if(it3->first == "first") {
                                auto val = (it3->second->as<std::string>())->get();
                                first = QString::fromUtf8(val.c_str());
                            }
                            if(it3->first == "second") {
                                auto val = (it3->second->as<std::string>())->get();
                                second = QString::fromUtf8(val.c_str());
                            }
                            if(it3->first == "k_elastic") {
                                auto val = (it3->second->as<double>())->get();
                                tn.ke = val;
                            }
                            if(it3->first == "x_rest") {
                                auto val = (it3->second->as<double>())->get();
                                tn.xr = val;
                            }
                            it3++;
                        }
                        auto fst = getObject(first);
                        auto sec = getObject(second);
                        tn.other = sec;
                        fst->elastic_ties.push_back(tn);
                    }
                    it2++;
                }
            }
        it1++;
    }
}

void Gravity::miau() {
    ui->lineEdit->setText("Miau.");
}

int nr = 0;

void Gravity::pas_fizica(double deltaT, bool tracktrail) {
    unsigned int i, j;
    for(i = 0; i < cosmicObjects.size(); i++)
        for(j = i + 1; j < cosmicObjects.size(); j++)
            cosmicObjects[i].interact_gravity(cosmicObjects[j], deltaT);
    for(i = 0; i < cosmicObjects.size(); i++)
        cosmicObjects[i].do_step(deltaT, tracktrail);
    for(i = 0; i < cosmicObjects.size(); i++)
        for(j = i + 1; j < cosmicObjects.size(); j++)
            cosmicObjects[i].check_collision(cosmicObjects[j]);
}

int nrsteps;

void Gravity::calculatestepnr() {
    double coef, coefloc;
    coef = 1.0E30;
    unsigned int i, j;
    for(i = 0; i < cosmicObjects.size(); i++)
        for(j = i + 1; j < cosmicObjects.size(); j++) {
            coefloc = fabs((cosmicObjects[i].pozitie - cosmicObjects[j].pozitie).sizesq()) / (fabs((cosmicObjects[i].viteza - cosmicObjects[j].viteza).size() * cosmicObjects[i].mass * cosmicObjects[j].mass));
            if(coefloc < coef && cosmicObjects[i].fictive == false && cosmicObjects[j].fictive == false)
                coef = coefloc;
        }
    if(coef > COEF_COEF)
        nrsteps = 1;
    else nrsteps = (COEF_COEF / coef);
    if(nrsteps > STEP_MAX_VALUE)
        nrsteps = STEP_MAX_VALUE;
}

void Gravity::step() {
    nr++;
    QString rez;
    rez += QString::number(nr);
    ui->lineEdit->setText(rez);
    mindinsubstep = 1.0E30;
    calculatestepnr();
    for(unsigned int i = 1; i <= nrsteps; i++)
        pas_fizica((0.001 * STEP_MS) / (1.0 * nrsteps), (i == 1));
}

float elapsedTimeInIntr;

void Gravity::intr_fizica() {
    QElapsedTimer tim;

    tim.start();
    if(!paused) {
        step();
    }

    elapsedTimeInIntr = tim.elapsed();

    this->repaint();

    t_fizica->start(STEP_MS);
}

int pg_x0 = 10, pg_y0 = 10, pg_x_len = 1000, pg_y_len = 1000;
double scale_x = 1, scale_y = 1;
CosmicObject *selectedObject, *centeredObject;
double offsetx, offsety;
enum trail_t {
    trail_00,
    trail_centered
} trail_type;

bool d_mass = true, d_vel = true, d_pos = true, d_vel_vec = false, d_radius = true, d_status;

double computePotentialEnergy() {
    double crt;
    crt = 0;
    int i, j;
    for(i = 0; i < cosmicObjects.size(); i++)
        if(!(cosmicObjects[i].destroyed))
            for(j = i + 1; j < cosmicObjects.size(); j++)
                if(!(cosmicObjects[j].destroyed))
                    crt -= GRAVITY_CONSTANT * cosmicObjects[i].mass * cosmicObjects[j].mass / (cosmicObjects[i].pozitie - cosmicObjects[j].pozitie).size();
    return crt;
}

double computeMinimumDistance() {
    double crt;
    crt = 1.0E30;
    int i, j;
    for(i = 0; i < cosmicObjects.size(); i++)
        if(!(cosmicObjects[i].destroyed))
            for(j = i + 1; j < cosmicObjects.size(); j++)
                if(!(cosmicObjects[j].destroyed))
                    if(crt > (cosmicObjects[i].pozitie - cosmicObjects[j].pozitie).size())
                        crt = (cosmicObjects[i].pozitie - cosmicObjects[j].pozitie).size();
    return crt;
}

double computeKineticEnergy() {
    double crt;
    crt = 0;
    int i, j;
    for(i = 0; i < cosmicObjects.size(); i++)
        if(!(cosmicObjects[i].destroyed))
            crt += cosmicObjects[i].mass * cosmicObjects[i].viteza.sizesq() / 2;
    return crt;
}

double computeTotalEnergy() {
    return computePotentialEnergy() + computeKineticEnergy();
}

double computeTotalImpulse() {
    vec2f crt;
    crt.x = 0;
    crt.y = 0;
    int i, j;
    for(i = 0; i < cosmicObjects.size(); i++)
        if(!(cosmicObjects[i].destroyed))
            crt = crt + cosmicObjects[i].viteza * cosmicObjects[i].mass;
    return crt.size();
}

void Gravity::paintEvent(QPaintEvent *event)
{

    QMainWindow::paintEvent(event);

    unsigned int i;
    QPainter painter(this);
    painter.fillRect(pg_x0, pg_y0, pg_x_len, pg_y_len, Qt::black);

    if(centeredObject == NULL) {
        offsetx = offsety = 0;
    } else {
        offsetx = -centeredObject->pozitie.x;
        offsety = -centeredObject->pozitie.y;
    }

    std::list<vec2f>::iterator it;
    vec2f prev;
    // Pasul 1: trailuri
    for(i = 0; i < cosmicObjects.size(); i++) {
        QColor color;
        color.setHsv((i * 59) % 256, 255 - (2 * i), 128);
        painter.setPen(color);

        std::list<vec2f>::iterator jt;
        if(centeredObject == NULL) {
            //
        } else {
            jt = centeredObject->prev_poz.begin();
        }
        for(it = cosmicObjects[i].prev_poz.begin(), prev = *it, it++; it != cosmicObjects[i].prev_poz.end(); it++) {
            double tox, toy;
            if(centeredObject == NULL) {
                tox = toy = 0;
            } else if(trail_type == trail_centered) {
                tox = -(jt->x);
                toy = -(jt->y);
                jt++;
            } else {
                tox = offsetx;
                toy = offsety;
            }
            painter.drawLine(pg_x0 + pg_x_len / 2 + (prev.x + tox) / scale_x, pg_y0 + pg_y_len / 2 + (prev.y + toy) / scale_y, pg_x0 + pg_x_len / 2 + (it->x + tox) / scale_x, pg_y0 + pg_y_len / 2 + (it->y + toy) / scale_y);
            prev = *it;
        }
    }
    // Pasul 2: Arcuri
    for(i = 0; i < cosmicObjects.size(); i++) {
        QColor color;
        color.setHsv((i * 59) % 256, (2 * i), 128);
        painter.setPen(color);

        std::vector<etie_t>::iterator jt;
        for(jt = cosmicObjects[i].elastic_ties.begin(); jt != cosmicObjects[i].elastic_ties.end(); jt++) {
            painter.drawLine(pg_x0 + pg_x_len / 2 + (cosmicObjects[i].pozitie.x + offsetx) / scale_x, pg_y0 + pg_y_len / 2 + (cosmicObjects[i].pozitie.y + offsety) / scale_y,
                             pg_x0 + pg_x_len / 2 + ((*jt).other->pozitie.x + offsetx) / scale_x, pg_y0 + pg_y_len / 2 + ((*jt).other->pozitie.y + offsety) / scale_y);
        }
    }
    // Pasul 3: Desenez obiectele propriu zise
    for(i = 0; i < cosmicObjects.size(); i++) {
        QColor color;
        QPen pen;
        color.setHsv((i * 59) % 256, 255 - (2 * i), 255);
        pen.setColor(color);
        pen.setWidth(3);
        painter.setPen(pen);

        float rad = cosmicObjects[i].radius / scale_x;
        if(rad < 2)
            rad = 2;
        else if(rad > 100)
            rad = 100;

        if(cosmicObjects[i].destroyed == false)
            painter.drawEllipse(QPointF(pg_x0 + pg_x_len / 2 + (cosmicObjects[i].pozitie.x + offsetx) / scale_x, pg_y0 + pg_y_len / 2 + (cosmicObjects[i].pozitie.y + offsety) / scale_y), rad, rad);
    }
    // Pasul 4: Informatia
    for(i = 0; i < cosmicObjects.size(); i++) {

        cosmicObjects[i].qtwi->setText(0, cosmicObjects[i].name.c_str());

        cosmicObjects[i].qtwi->setText(1, QString::number(cosmicObjects[i].mass));
        cosmicObjects[i].qtwi->setText(2, QString::number(cosmicObjects[i].viteza.size()));
        cosmicObjects[i].qtwi->setText(3, QString::number(cosmicObjects[i].pozitie.x) + ", " + QString::number(cosmicObjects[i].pozitie.y));
        cosmicObjects[i].qtwi->setText(4, QString::number(cosmicObjects[i].viteza.x) + ", " + QString::number(cosmicObjects[i].viteza.y));
        cosmicObjects[i].qtwi->setText(5, QString::number(cosmicObjects[i].radius));
        QColor redc(255, 0, 0), blackc(0, 0, 0), greenc(0, 125, 0);
        if(cosmicObjects[i].destroyed) {
            cosmicObjects[i].qtwi->setText(6, "Destroyed");
            cosmicObjects[i].qtwi->setTextColor(6, redc);
        } else if(cosmicObjects[i].fictive) {
            cosmicObjects[i].qtwi->setText(6, "Fictive");
            cosmicObjects[i].qtwi->setTextColor(6, blackc);
        } else {
            cosmicObjects[i].qtwi->setText(6, "Normal");
            cosmicObjects[i].qtwi->setTextColor(6, greenc);
        }

        //cosmicObjects[i].qlwi->setText(QString("") + cosmicObjects[i].name.c_str() + " - poz(" + QString::number(cosmicObjects[i].pozitie.x) + ", " + QString::number(cosmicObjects[i].pozitie.y) + "), vel(" + QString::number(cosmicObjects[i].viteza.x) + ", " + QString::number(cosmicObjects[i].viteza.y) + "), mass = " + QString::number(cosmicObjects[i].mass));
    }

    ui->objectList->setColumnHidden(1, !d_mass);
    ui->objectList->setColumnHidden(2, !d_vel);
    ui->objectList->setColumnHidden(3, !d_pos);
    ui->objectList->setColumnHidden(4, !d_vel_vec);
    ui->objectList->setColumnHidden(5, !d_radius);

    ui->objectList->header()->resizeSections(QHeaderView::Stretch);

    ui->valueDials->setText(QString("Total Energy: ") + QString::number(computeTotalEnergy()) + "\n" +
                            "Potential Energy: " + QString::number(computePotentialEnergy()) + "\n" +
                            "Kinetic Energy: " + QString::number(computeKineticEnergy()) + "\n" +
                            "Total Impulse Value: " + QString::number(computeTotalImpulse()) + "\n" +
                            "Substep Number: " + QString::number(nrsteps) + "\n" +
                            "Minimum Distance: " + QString::number(computeMinimumDistance()) + '\n' +
                            "Minimum Distance in Substep: " + QString::number(mindinsubstep));

    if(!paused)
        ui->pauseButton->setText("❚❚");
    else ui->pauseButton->setText("▶");

    float loadpercent = (elapsedTimeInIntr / STEP_MS) * 100.0;

    ui->cpuLoadLabel->setText("Load: " + QString::number(int(loadpercent)) + "%");
    if(loadpercent < 50)
        ui->cpuLoadLabel->setStyleSheet("QLabel { color: green; }");
    else if(loadpercent < 90)
        ui->cpuLoadLabel->setStyleSheet("QLabel { color: gold; }");
    else ui->cpuLoadLabel->setStyleSheet("QLabel { color: red; }");
}

argList parseCommand(QString command) {
    argList crtq;
    QString crt;
    int i, lp;
    lp = 0;
    command = command.simplified();
    for(i = 0; i < command.size(); i++)
        if(command[i] == '\\')
            crt += command[++i];
        else if(command[i] == ' ') {
            crtq.push_back(crt);
            crt = "";
        } else if(command[i] == '"') {
            for(i++; i < command.size(); i++)
                if(command[i] == '\\')
                    crt += command[++i];
                else if(command[i] == '"') {
                    crtq.push_back(crt);
                    crt = "";
                    i++;
                } else crt += command[i];
        } else crt += command[i];
    if(crt != "")
        crtq.push_back(crt);
    return crtq;
}

QString commandOutput;

#define CPRINT commandOutput=commandOutput+

CosmicObject *Gravity::getObject(QString s) {
    auto it = cosmicObjects.begin();
    CosmicObject *res;
    for(; it != cosmicObjects.end(); it++)
        if(s == (it->name).c_str()) {
            res = &(*it);
            break;
        }
    return res;
}

void Gravity::selectObject(argList args) {
    auto it = cosmicObjects.begin();
    if(args.size() < 2) {
        CPRINT "Not enough arguments\nUsage:\nselect <number>\n\tSelects the nth object (DEBUGGING PURPOSES)\nselect <name>\n\tSelects the object with the name <name>\n";
        return;
    }
    if(args[1][0] >= '0' && args[1][0] <= '9' || args[1][0] == '-' || args[1][0] == '+') {
        int i, poz;
        poz = args[1].toDouble();
        if(poz < 0) {
            CPRINT "Invalid number";
            return;
        }
        for(i = 0; i < poz; i++, it++)
            if(it == cosmicObjects.end()) {
                break;
            }
        if(it == cosmicObjects.end()) {
            CPRINT "Out of range";
        } else {
            selectedObject = &(*it);
            CPRINT "Selected object \"" + (selectedObject->name).c_str() + "\"";
        }
    } else {
        if(args[1] != "")
        for(; it != cosmicObjects.end(); it++)
            if(args[1] == (it->name).c_str()) {
                selectedObject = &(*it);
                CPRINT "Selected object \"" + (selectedObject->name).c_str() + "\"";
            }
    }
    CPRINT "\n";
}

void Gravity::centerObject(argList args) {
    auto it = cosmicObjects.begin();
    if(args.size() < 2) {
        CPRINT "Not enough arguments\nUsage:\ncenter <number>Centers the nth object (DEBUGGING PURPOSES)\ncenter <name>\n\tCenters the object with the name <name>\n";
        return;
    }
    if(args[1][0] >= '0' && args[1][0] <= '9' || args[1][0] == '-' || args[1][0] == '+') {
        int i, poz;
        poz = args[1].toDouble();
        if(poz < 0) {
            CPRINT "Invalid number";
            return;
        }
        for(i = 0; i < poz; i++, it++)
            if(it == cosmicObjects.end()) {
                break;
            }
        if(it == cosmicObjects.end()) {
            CPRINT "Out of range";
        } else {
            centeredObject = &(*it);
            CPRINT "Centered object \"" + (centeredObject->name).c_str() + "\"";
        }
    } else {
        if(args[1] != "")
        for(; it != cosmicObjects.end(); it++)
            if(args[1] == (it->name).c_str()) {
                centeredObject = &(*it);
                CPRINT "Centered object \"" + (centeredObject->name).c_str() + "\"";
            }
    }
    CPRINT "\n";
}

void Gravity::clear(argList args) {
    commandOutput = "";
}

void setzoom(argList args) {
    if(args.size() < 2) {
        CPRINT "Not enough arguments\nUsage:\nzoom <x> [<y>]\n";
        return;
    }
    if(args.size() < 3) {
        scale_x = scale_y = args[1].toDouble();
    } else {
        scale_x = args[1].toDouble();
        scale_y = args[2].toDouble();
    }
    if(scale_x == 0) scale_x = 1;
    if(scale_y == 0) scale_y = 1;
}

#define SELECT_CHECK if(selectedObject==NULL){CPRINT "Must have object selected\n"; return;}
#define CENTER_CHECK if(centeredObject==NULL){CPRINT "Must have object selected\n"; return;}

void setmass(argList args) {
    SELECT_CHECK
    if(args.size() < 2) {
        CPRINT "Not enough arguments\nUsage:\nsetmass <mass>\n";
        return;
    }
    selectedObject->mass = args[1].toDouble();
}

void setx(argList args) {
    SELECT_CHECK
    if(args.size() < 2) {
        CPRINT "Not enough arguments\nUsage:\nsetx <x>\n";
        return;
    }
    selectedObject->pozitie.x = args[1].toDouble();
}
void sety(argList args) {
    SELECT_CHECK
    if(args.size() < 2) {
        CPRINT "Not enough arguments\nUsage:\nsety <y>\n";
        return;
    }
    selectedObject->pozitie.y = args[1].toDouble();
}

void setvx(argList args) {
    SELECT_CHECK
    if(args.size() < 2) {
        CPRINT "Not enough arguments\nUsage:\nsetvx <vx>\n";
        return;
    }
    selectedObject->viteza.x = args[1].toDouble();
}
void setvy(argList args) {
    SELECT_CHECK
    if(args.size() < 2) {
        CPRINT "Not enough arguments\nUsage:\nsetvy <vy>\n";
        return;
    }
    selectedObject->viteza.y = args[1].toDouble();
}

void togglefictive(argList args) {
    SELECT_CHECK
    selectedObject->fictive = !(selectedObject->fictive);
    if(selectedObject->fictive)
        CPRINT selectedObject->name.c_str() + " is now fictive\n";
}

void Gravity::addobject(argList args) {
    CosmicObject newo;
    ui->objectList->addTopLevelItem(newo.qtwi);
    newo.fictive = true;
    if(args.size() != 2 && args.size() != 7)
        CPRINT "Not enough arguments\nUsage:\naddobject <name> [<mass> <x> <y> <vx> <vy>]\n";
    if(args.size() == 2) {
        newo.name = args[1].toLatin1().data();
        newo.mass = 1;
        newo.pozitie.x = newo.pozitie.y = 0;
        newo.viteza.x = newo.viteza.y = 0;
        cosmicObjects.push_back(newo);
        CPRINT "Added object with name " + newo.name.c_str() + ", mass = 1, speed 0 and at (0,0)\n";
    } else if(args.size() == 7) {
        newo.name = args[1].toLatin1().data();
        newo.mass = args[2].toDouble();
        newo.pozitie.x = args[3].toDouble();
        newo.pozitie.y = args[4].toDouble();
        newo.viteza.x = args[5].toDouble();
        newo.viteza.y = args[6].toDouble();
        cosmicObjects.push_back(newo);
        CPRINT "Added object with name " + newo.name.c_str() + ", mass = " + QString::number(newo.mass) + ", speed (" + QString::number(newo.viteza.x) + "," + QString::number(newo.viteza.y) + ") and at (" + QString::number(newo.pozitie.x) + "," + QString::number(newo.pozitie.y) + ")\n";
    }
}

void change_trail_type(argList args) {
    if(args.size() < 2) {
        CPRINT "Not enough arguments\nUsage:\n\ttrail <global | 00 | centered | ct>\n";
    } else {
        if(args[1] == "global" || args[1] == "00")
            trail_type = trail_00;
        else if(args[1] == "centered" || args[1] == "ct")
            trail_type = trail_centered;
        else CPRINT "Unknown trail type \"" + args[1] + "\"\n";
    }
}

void Gravity::add_elastic_tie(argList args) {
    CosmicObject *a, *b;
    etie_t tmp;
    if(args.size() != 5) {
        CPRINT "Invalid number of arguments\nUsage:\n\ttie <obj1> <obj2> <k_elastic> <x_rest>";
        return;
    }
    a = getObject(args[1]);
    b = getObject(args[2]);
    tmp.other = b;
    tmp.ke = args[3].toDouble();
    tmp.xr = args[4].toDouble();
    a->elastic_ties.push_back(tmp);
}

void Gravity::on_command() {
    QString crt;
    crt = ui->commandEdit->text();
    ui->commandEdit->setText("");
    commandOutput += ">" + crt + "\n";
    // Processing
    argList args;
    args = parseCommand(crt);
    if(args.size() == 0) {
    } else if(args[0] == "select")
        selectObject(args);
    else if(args[0] == "clear")
        clear(args);
    else if(args[0] == "noselect")
        selectedObject = NULL;
    else if(args[0] == "setzoom")
        setzoom(args);
    else if(args[0] == "setmass")
        setmass(args);
    else if(args[0] == "setx")
        setx(args);
    else if(args[0] == "sety")
        sety(args);
    else if(args[0] == "setvx")
        setvx(args);
    else if(args[0] == "setvy")
        setvy(args);
    else if(args[0] == "togglefictive" || args[0] == "tf")
        togglefictive(args);
    else if(args[0] == "addobject" || args[0] == "addo")
        addobject(args);
    else if(args[0] == "pause" || args[0] == "p")
        paused = !paused;
    else if(args[0] == "trail")
        change_trail_type(args);
    else if(args[0] == "center")
        centerObject(args);
    else if(args[0] == "nocenter")
        centeredObject = NULL;
    else if(args[0] == "tie")
        add_elastic_tie(args);
    else CPRINT "Invalid command \"" + args[0] + "\"\n";
    // /Processing
    ui->results->setText(commandOutput);
    ui->results->verticalScrollBar()->setValue(ui->results->verticalScrollBar()->maximum());
}

Gravity::~Gravity()
{
    delete ui;
}

void Gravity::on_pauseButton_clicked()
{
    paused = !paused;
}

void Gravity::on_nextStepButton_clicked()
{
    step();
}
