#-------------------------------------------------
#
# Project created by QtCreator 2015-07-30T16:07:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Gravity
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        gravity.cpp

HEADERS  += gravity.h \
    cpptoml.h

FORMS    += gravity.ui
